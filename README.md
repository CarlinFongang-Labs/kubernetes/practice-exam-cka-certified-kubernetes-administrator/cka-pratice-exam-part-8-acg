# 8. CKA Pratice Exam Part 8 ACG : NetworkPolicy

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectif 

1. Créer une stratégie réseau qui refuse tout accès aux pods du namespace maintenance

2. Créez une stratégie de réseau qui permet à tous les pods de l'espace de noms users-backend de communiquer entre eux uniquement sur un port spécifique.

# Context

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées, ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le véritable examen CKA. Bonne chance!


Cette question utilise le acgk8scluster. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud, c'est-à-dire ssh acgk8s-worker1.

Remarque : Vous ne pouvez pas vous connecter à un autre nœud, ni l'utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de exitrevenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé à /home/cloud_user/verify.shtout moment pour vérifier votre travail !

>![Alt text](img/image.png)

# Application

## Étape 1 : Connexion au serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Créer une stratégie réseau qui refuse tout accès au Pod du namespace maintenance

1. Passez au contexte `acgk8s` :

```sh
kubectl config use-context acgk8s
```

2. Vérifiez les pods dans l'espace de noms `foo` :

```sh
kubectl get pods -n foo
```

3. Vérifiez les étiquettes du pod `maintenance` :

```sh
kubectl describe pod maintenance -n foo
```

4. Créez un nouveau fichier YAML nommé `np-maintenance.yml` :

```sh
vim np-maintenance.yml
```

5. Dans le fichier, collez ce qui suit :

```yaml
---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: np-maintenance  
  namespace: foo
spec:
  podSelector:
    matchLabels:
      app: maintenance
  policyTypes:
  - Ingress
  - Egress
```

6. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

7. Créez le NetworkPolicy :

```sh
kubectl create -f np-maintenance.yml
```

8. Vérifiez l'état des objectifs de l'examen :

```sh
 kubectl get networkpolicy -n foo
```

```sh
./verify.sh
```

>![Alt text](img/image-1.png)

## Étape 3 : Créer une stratégie réseau qui permet à tous les pods de l'espace de noms `users-backend` de communiquer entre eux uniquement sur un port spécifique

1. Étiquetez l'espace de noms `users-backend` :

```sh
kubectl label namespace users-backend app=users-backend
```

>![Alt text](img/image-2.png)

2. Créez un fichier YAML nommé `np-users-backend-80.yml` :

```sh
vim np-users-backend-80.yml
```

3. Dans le fichier, collez ce qui suit :

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
    name: np-users-backend-80
    namespace: users-backend
spec:
    podSelector: {}
    policyTypes:
    - Ingress
    ingress:
    - from:
    - namespaceSelector:
        matchLabels:
            app: users-backend
    ports:
    - protocol: TCP
        port: 80
```

Ref doc : 

https://kubernetes.io/docs/concepts/services-networking/network-policies/#targeting-a-range-of-ports

https://kubernetes.io/docs/concepts/services-networking/network-policies/#behavior-of-to-and-from-selectors

4. Enregistrez le fichier :

   - Appuyez sur `ESC`
   - Tapez `:wq` et appuyez sur `Entrée`

5. Créez la stratégie réseau :

```sh
kubectl create -f np-users-backend-80.yml
```

6. Vérifiez l'état des objectifs de l'examen :

```sh
kubectl get networkpolicy -n users-backend
```

```sh
./verify.sh
```

>![Alt text](img/image-3.png)

En suivant ces étapes, vous aurez créé des stratégies réseau pour restreindre l'accès au pod de maintenance et permettre la communication entre les pods dans l'espace de noms `users-backend` uniquement sur le port 80.